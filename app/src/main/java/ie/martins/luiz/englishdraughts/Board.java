package ie.martins.luiz.englishdraughts;

//----This class is where we create the bidimensional board and define the initial positions of the stones/pieces/pion
public class Board {

    // ----The following variable is accessed from the MainActivity to define the board size
    //     8 == 8x8 which is the default(initial) size ----
    static public int boardSize = 8;

    //The bidimensional board is being created
    static public Cell board[][];

    public Board() {
        board = new Cell[boardSize][boardSize];
        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                board[x][y] = new Cell(x, y);
                // ----The stones are drawn on the green cells only, the second part will draw the white and red stones,
                //     two rows between the stones will be empty, no stones will be drawn in there
                //     the ideia is to divide the board in the middle and remove one row up and one down----
                if (returnOnlyIfGreenCell(x, y) && (y < ((boardSize/2) -1) || y > (boardSize - ((boardSize/2))))){
                    if (y > (boardSize - ((boardSize/2)))) {
                        board[x][y].setStone(Stone.returnRedStone());
                    } else {
                        board[x][y].setStone(Stone.returnWhiteStone());
                    }
                }
            }
        }
    }

    // The board's available positions should look like that, if it is board size == 6
    // 0x5, 1x5 , 2x5 , 3x5 , 4x5, 5,5
    // 0x4, 1x4 , 2x4 , 3x4 , 4x4, 5,4
    // 0x3, 1x3 , 2x3 , 3x3 , 4x3, 5,3
    // 0x2, 1x2 , 2x2 , 3x2 , 4x2, 5,2
    // 0x1, 1x1 , 2x1 , 3x1 , 4x1, 5,1
    // 0x0, 1x0 , 2x0 , 3x0 , 4x0, 5,0

    public static Cell returnCellFromBoard(int x, int y) {
        // ---- Return a specific position, it can be used to check if there is a stone on that position ----
        return board[x][y];
    }

    public static boolean returnOnlyIfValidCell(int x, int y) {
        // ---- Is a valid cell if between [0,0] and [8,8], to avoid out of bounds ----
        return x >= 0 && y >= 0 && x < boardSize && y < boardSize;
    }

    public static boolean returnOnlyIfGreenCell(int x, int y) {
        // ---- That's the opposite of the yellow cells, the green ones are always even numbers(x+y) ----
        return (x + y) % 2 == 0;
    }

    public static boolean returnOnlyIfYellowCell(int x, int y) {
        // ---- The result of x+y on the yellow cells are always odd numbers ----
        return (x + y) % 2 != 0;
    }
}
