package ie.martins.luiz.englishdraughts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

//----This is the Game view(board area), here is where the styles of the drawings are defined
//   and the onTouchEvent which is the trigger for the whole program----

public class GameView extends View {

//*** I've put the variables on the bottom ****

    //----It will make the view square in size----
    @Override
    final public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int gameViewDiameter;
        int widthScreenSize = getMeasuredWidth();
        int heightScreenSize = getMeasuredHeight();
        //---- The gameViewDiameter of the view will be based on the lowest side, widthScreenSize or heightScreenSize ----
        if (widthScreenSize < heightScreenSize) {
            gameViewDiameter = widthScreenSize;
        } else  {
            gameViewDiameter = heightScreenSize;
        }
        //---- Copying the onMeasure "gameViewDiameter" to the "gameViewDiameter" on the MainActivity to be used on the NEW GAME buttons. ----
        MainActivity.gameViewDiameter = gameViewDiameter;
        GameView.super.setLayoutParams(new LinearLayout.LayoutParams(gameViewDiameter, gameViewDiameter));
        //---- It will define the size of each cell which will depend on the size of the board ----
        cellSize = gameViewDiameter / board.boardSize;
        //---- setShadowLayer() is only supported on text when hardware acceleration is on. Hardware acceleration
        // is on by default when targetSdk=14 or higher. An easy workaround is to put your View in a software layer ----
        GameView.this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    //---- A constructor is needed, otherwise we will get an exception when Android tries to inflate the View. ----
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        new GameStart();
        //---- It will do additional constructor work ----
        gameViewPaints();
    }

    //---- Continuation, here we define all the styling for the Paints, like colors, shadow, etc. ----
    private void gameViewPaints() {
        stoneSelectedPaint = new Paint(); cellSelectedPaint = new Paint(); cellGraySelectedPaint = new Paint();
        bgGreenPaint = new Paint(); bgYellowPaint = new Paint(); bgBlackPaint = new Paint(); bgWhitePaint = new Paint();
        stoneRedPaint = new Paint(); stoneWhitePaint = new Paint();
        stoneQueenBorderPaint = new Paint();
        bgGreenPaint.setARGB(255, 73, 120, 0); //Green
        bgGreenPaint.setStyle(Paint.Style.FILL);
        bgYellowPaint.setARGB(255, 231, 197, 133); //Yellowish
        bgYellowPaint.setStyle(Paint.Style.FILL);
        bgBlackPaint.setARGB(255, 0, 0, 0); //Black
        bgBlackPaint.setStyle(Paint.Style.FILL);
        bgWhitePaint.setARGB(255, 255, 255, 255); //White
        bgWhitePaint.setStyle(Paint.Style.FILL);
        stoneRedPaint.setARGB(255, 175, 0, 0); //Red
        stoneRedPaint.setStyle(Paint.Style.FILL);
        stoneRedPaint.setShadowLayer(10, 4, 4, 0xFF000000); //(float radius, float dx, float dy, int shadowColor) | Black
        stoneRedPaint.setAntiAlias(true);
        stoneWhitePaint.setARGB(255, 255, 255, 255); //White
        stoneWhitePaint.setStyle(Paint.Style.FILL);
        stoneWhitePaint.setShadowLayer(10, 4, 4, 0xFF000000); //(float radius, float dx, float dy, int shadowColor) | Black
        stoneWhitePaint.setAntiAlias(true);
        stoneSelectedPaint.setARGB(255, 252, 210, 9); //Yellow
        stoneSelectedPaint.setStyle(Paint.Style.FILL);
        stoneSelectedPaint.setShadowLayer(2, 4, 4, 0xFFFFFFFF); //(float radius, float dx, float dy, int shadowColor) | White
        stoneSelectedPaint.setAntiAlias(true);
        stoneQueenBorderPaint.setARGB(255, 61, 159, 230); //Blue
        stoneQueenBorderPaint.setStyle(Paint.Style.FILL);
        stoneQueenBorderPaint.setShadowLayer(2, 4, 4, 0xFF000000); //(float radius, float dx, float dy, int shadowColor) | Black
        stoneQueenBorderPaint.setAntiAlias(true);
        cellSelectedPaint.setARGB(255, 16, 65, 0); //Dark Green
        cellSelectedPaint.setStyle(Paint.Style.FILL);
        cellSelectedPaint.setShadowLayer(10, 0, 0, 0xFF000000); //(float radius, float dx, float dy, int shadowColor) | Black
        cellSelectedPaint.setAntiAlias(true);
        cellGraySelectedPaint.setARGB(255, 127, 140, 140); //Gray
        cellGraySelectedPaint.setStyle(Paint.Style.FILL);
        cellGraySelectedPaint.setShadowLayer(10, 0, 0, 0xFF000000); //(float radius, float dx, float dy, int shadowColor) | Black
        cellGraySelectedPaint.setAntiAlias(true);
    }

    //----It will be called by the onDraw, it will draw the cells(squares)----
    private void drawCell(Canvas canvas, int x, int y) {
        Paint paint;

        //----l = left side, starting point of the drawing of each cell----
        int l = x * cellSize;
        //----t = top side, starting point of the drawing of each cell. The -1 is because we start from the highest to lowest
        //    and the array finishes at board.boardSize -1----
        int t = (board.boardSize - y - 1) * cellSize;

        //----Depending on the text of the DarkForces button on the MainActivity different colours will be drawn,
        //    the ideia is simple this part of the code should be optimized----
        if (Board.returnOnlyIfYellowCell(x, y) && MainActivity.darkForces.getText().equals("DARK FORCES OFF")) {
            paint = bgYellowPaint;
            //----The cells are drawn from the top left to the bottom right.----
            canvas.drawRect(l, t, l + cellSize, t + cellSize, paint);
        }
        if (Board.returnOnlyIfYellowCell(x, y) && MainActivity.darkForces.getText().equals("DARK FORCES ON")) {
            paint = bgWhitePaint;
            canvas.drawRect(l, t, l + cellSize, t + cellSize, paint);
        }
        if (Board.returnOnlyIfGreenCell(x, y) && MainActivity.darkForces.getText().equals("DARK FORCES OFF")) {
            paint = bgGreenPaint;
            canvas.drawRect(l, t, l + cellSize, t + cellSize, paint);
        }
        if (Board.returnOnlyIfGreenCell(x, y) && MainActivity.darkForces.getText().equals("DARK FORCES ON")) {
            paint = bgBlackPaint;
            canvas.drawRect(l, t, l + cellSize, t + cellSize, paint);
        }
        if (GameStart.returnOnlyIfSelectedCell(GameRules.returnBoard().returnCellFromBoard(x, y)) && MainActivity.darkForces.getText().equals("DARK FORCES OFF")) {
            paint = cellSelectedPaint;
            canvas.drawRect(l, t, l + cellSize, t + cellSize, paint);
        }
        if (GameStart.returnOnlyIfSelectedCell(GameRules.returnBoard().returnCellFromBoard(x, y)) && MainActivity.darkForces.getText().equals("DARK FORCES ON")) {
            paint = cellGraySelectedPaint;
            canvas.drawRect(l, t, l + cellSize, t + cellSize, paint);
        }
    }

    //----It will be called by onDraw to draw the stones----
    private void drawStone(Canvas canvas, int x, int y) {
        if (GameRules.returnBoard().returnCellFromBoard(x, y).returnOnlyIfFreeCell()) {
            return;
        }
        Paint paint = new Paint();
        //----When the stone becomes queen a different border is drawn on it----
        Paint paintBorder;

        //----This position refers to the middle of each cell on the board on the X axis, the middle of the cell is based on the
        //    cellSize / 2 in the end----
        int xStoneCoordinates = x * cellSize + cellSize / 2;
        //----This position refers to the middle of each cell on the board on the Y axis----
        int yStoneCoordinates = (board.boardSize - y - 1) * cellSize + cellSize / 2;

        // ---- Check if the stone is red or white to define the paint which will be used----
        if (GameRules.returnBoard().returnCellFromBoard(x, y).returnStone().returnOnlyIfRed()) {
            paint = stoneRedPaint;
        }
        if (GameRules.returnBoard().returnCellFromBoard(x, y).returnStone().returnOnlyIfWhite()) {
            paint = stoneWhitePaint;
        }
        // ---- It will draw the stone with two circles one inside another: drawCircle(x, y, radius, paint), because of shadow
        //      it looks like 3D ----
        canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 2.5), paint);
        canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 3.2), paint);

        // ---- The following part will draw the queen stones when they are present, the difference is the paintBorder ----
        if (GameRules.returnBoard().returnCellFromBoard(x, y).returnStone().returnOnlyIfRedQueen()) {
            paint = stoneRedPaint;
            paintBorder = stoneQueenBorderPaint;
            canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 2.5), paintBorder);
            canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 3.2), paint);
        }
        if (GameRules.returnBoard().returnCellFromBoard(x, y).returnStone().returnOnlyIfWhiteQueen()) {
            paint = stoneWhitePaint;
            paintBorder = stoneQueenBorderPaint;
            canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 2.5), paintBorder);
            canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 3.2), paint);
        }

        // ---- If the cell is selected it will be painted with a different paint ----
        if (GameStart.returnOnlyIfSelectedCell(GameRules.returnBoard().returnCellFromBoard(x, y))) {
            // ---- The only difference in here is the paint = stoneSelectedPaint ----
            paint = stoneSelectedPaint;
            xStoneCoordinates = x * cellSize + cellSize / 2;
            yStoneCoordinates = (board.boardSize - y - 1) * cellSize + cellSize / 2;
            // It will draw two circles one inside another: drawCircle(x, y, radius, paint)
            canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 2.5), paint);
            canvas.drawCircle(xStoneCoordinates, yStoneCoordinates, (float) (cellSize / 3.2), paint);
        }
    }

    // ---- It will draw the cells and the stones for the whole game board, it is limited at the
    //      "boardSize"(could be 6, 8 or 20. It will call the drawCell and drawStone updating all positions----
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int x = 0; x < board.boardSize; x++) {
            for (int y = 0; y < board.boardSize; y++) {
                drawCell(canvas, x, y);
                drawStone(canvas, x, y);
            }
        }
    }

    // ---- This part is responsible for the onTouchEvent, when we touch the screen ----
    @Override
    public boolean onTouchEvent(MotionEvent event) {
		/*----the ACTION_DOWN event indicates that the user has placed their first finger
		on the device and has initiated the first touch, any other MotionEvent is discarded ---- */
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            // ---- It will get the touch position based on the boardSize.The event.getX and event.getY equal to x0,y0 it the t l----
            // ---- To make the y0 on the bottom we need to normalize and subtract its coordinates from the max height(boardSize)----
            int xTouchCoordinates = (int) (event.getX() / cellSize);
            int yTouchCoordinates = (board.boardSize - 1) - (int) (event.getY() / cellSize);

            // ---- Playing the sound.mp3 from the raw folder when there is a touch event, when the DF is enabled the sound is different----
            if ( MainActivity.darkForces.getText().equals("DARK FORCES ON")) {
                sound = MediaPlayer.create(mainActivityContextThis, R.raw.saber);
            }
            else {
                sound = MediaPlayer.create(mainActivityContextThis, R.raw.sound);
            }
            // ---- It will play the sound ----
            sound.start();
            // ---- It will release the player, which was stopping after 28 plays ----
            sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer sound) {
                    sound.release();
                }
            });

            //It will use the touch x and y coordinates to execute the actions
            GameStart.touchAction(xTouchCoordinates, yTouchCoordinates);

            //----It will update the Red and White stone count every touch ----
            GameRules.returnRedStoneCount();
            GameRules.returnWhiteStoneCount();

            // ---- Those rules will display the updated stone count, turn and game status ----
            MainActivity.redPlayerCount.setText("RED: " + redStoneCount);
            MainActivity.whitePlayerCount.setText("WHITE: " + whiteStoneCount);
            if (PlayerEnum.returnInversePlayer(GameRules.currentPlayer) == PlayerEnum.WHITE){
                MainActivity.gameStatus.setText("Turn: " + PlayerEnum.returnInversePlayer(GameRules.currentPlayer));
                MainActivity.gameStatus.setTextColor(Color.WHITE);
            }
            if (PlayerEnum.returnInversePlayer(GameRules.currentPlayer) == PlayerEnum.RED){
                MainActivity.gameStatus.setText("Turn: " + PlayerEnum.returnInversePlayer(GameRules.currentPlayer));
                MainActivity.gameStatus.setTextColor(Color.RED);
            }
            if (redStoneCount == 0) {
                MainActivity.gameStatus.setText("WHITE WINS!!!");
                MainActivity.gameStatus.setTextColor(Color.WHITE);
                MainActivity.chronometer.stop();
            }
            if (whiteStoneCount == 0) {
                MainActivity.gameStatus.setText("RED WINS!!!");
                MainActivity.gameStatus.setTextColor(Color.RED);
                MainActivity.chronometer.stop();
            }
            // The invalidate() will force the view to draw calling the onDraw
            invalidate();
            return true;
        }
        else {
            return false;
        }
    }

    private int cellSize;
    private Paint stoneSelectedPaint, cellSelectedPaint, cellGraySelectedPaint;
    private Paint bgGreenPaint, bgYellowPaint, bgBlackPaint, bgWhitePaint;
    private Paint stoneWhitePaint, stoneRedPaint;
    private Paint stoneQueenBorderPaint;
    private Board board;
    static MediaPlayer sound = new MediaPlayer();
    static Context mainActivityContextThis;
    static Integer redStoneCount, whiteStoneCount;

}
