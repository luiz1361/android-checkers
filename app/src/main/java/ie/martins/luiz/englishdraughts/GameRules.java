package ie.martins.luiz.englishdraughts;

// ---- Hashsets are being used instead of arrays basically to avoid duplicated values
import java.util.HashSet;

// ---- Here is where all game rules are defined ----
public class GameRules {

    static private Board board;
    // ---- The movementStonesHashSet keeps tracks of the valid positions on the board for the current player
    //      where he/she can touch ----
    static public HashSet movementStonesHashSet;
    static public PlayerEnum currentPlayer;
    // Indicates that we are in hungryMode, which means multiple eat
    static private boolean hungryMode;

    public GameRules() {
        board = new Board();
        movementStonesHashSet = new HashSet();
        currentPlayer = PlayerEnum.WHITE;
        hungryMode = false;
        movementStonesHashSet.clear();
        // ---- The following method will populate the movementStonesHashSet
        includeValidMovementStones();
    }

    // ---- It will return the valid positions to click on the board for the current player ----
    static public HashSet returnMoveStones() {
        return movementStonesHashSet;
    }

    // ---- Quick way to get access to the Board() ----
    static public Board returnBoard() {
        return board;
    }

    // ---- It will return true or false if we are in HungryMode(Multiple eat) ----
    static public boolean returnIfHungryMode() {
        return hungryMode;
    }

    // ---- It will update the valid free/empty cells with the the Board
    static private boolean includeCellToHashSet(int x, int y, HashSet cellsHashSet) {
        if (board.returnOnlyIfValidCell(x, y) && board.returnCellFromBoard(x, y).returnOnlyIfFreeCell()) {
            if (cellsHashSet != null)
                cellsHashSet.add(board.returnCellFromBoard(x, y));
            return true;
        }
        return false;
    }

    // ---- It will define the direction that the player is allowed to move/eat top or down, if the stone is red it can go
    //      down -1 else up 1----
    static private int returnVerticalDirection(Cell cell) {
        if (cell.returnStone().returnOnlyIfRed()) {
            //---- If normal RED stone the valid direction is down in the Y axis ----
            return -1;
        }
        else {
            //---- If the normal WHITE stone the valid direction is up in the Y axis
            return 1;
        }
    }

    // ---- This method is responsible to populate the
    static public boolean includeValidEatCells(Cell selectedCell, HashSet Cells) {
        if (selectedCell == null) {
            return false;
        }
        // ---- If the selectedcell is a queen call the includevalideatcellsqueen instead ----
        if ( selectedCell.returnStone().returnOnlyIfWhiteQueen() || selectedCell.returnStone().returnOnlyIfRedQueen() ){
            includeValidEatCellsQueen(selectedCell, Cells);
            return false;
        }
        int x = selectedCell.returnXpos();
        int y = selectedCell.returnYpos();
        //---- Those are the normal stones valid eat directions ----
        int yDirection = returnVerticalDirection(selectedCell);
        PlayerEnum oppositePlayer = PlayerEnum.returnInversePlayer(selectedCell.returnStone().returnPlayer());
        boolean result = includeValidEatCells(x, y, 1, yDirection, oppositePlayer, Cells);
        boolean result2 =  includeValidEatCells(x, y, -1, yDirection, oppositePlayer, Cells);
        return result || result2;
    }

    // ---- This method is called from the method above, it will define which stones(types) the player can eat and multiply
    //      the posible directions x and y by 2 which is the destination that the stone will go after the eat ----
    static private boolean includeValidEatCells(int x, int y, int xDirection, int yDirection, PlayerEnum inversePlayer, HashSet Cells) {
        int horizontalNeighbour = x + xDirection;
        int verticalNeighbour = y + yDirection;
        int eatX = x + 2 * xDirection;
        int eatY = y + 2 * yDirection;

        if (board.returnOnlyIfValidCell(horizontalNeighbour, verticalNeighbour) && board.returnCellFromBoard(horizontalNeighbour, verticalNeighbour).returnOnlyIfhasPlayer(inversePlayer)) {
            return includeCellToHashSet(eatX, eatY, Cells);
        }
        // ---- When the opponent is RED you can eat REDQUEEN as well ----
        if (inversePlayer == PlayerEnum.RED) {
            if (board.returnOnlyIfValidCell(horizontalNeighbour, verticalNeighbour) && board.returnCellFromBoard(horizontalNeighbour, verticalNeighbour).returnOnlyIfhasPlayer(PlayerEnum.REDQUEEN)) {
                return includeCellToHashSet(eatX, eatY, Cells);
            }
        }
        // ---- When the opponent is WHITE you can eat WHITEQUEEN as well ----
        if (inversePlayer == PlayerEnum.WHITE) {
            if (board.returnOnlyIfValidCell(horizontalNeighbour, verticalNeighbour) && board.returnCellFromBoard(horizontalNeighbour, verticalNeighbour).returnOnlyIfhasPlayer(PlayerEnum.WHITEQUEEN)) {
                return includeCellToHashSet(eatX, eatY, Cells);
            }
        }
        return false;
    }

    // ---- The same ideia of the method above but including the possible eats for the queen, up and down----
    static public boolean includeValidEatCellsQueen(Cell selectedCell, HashSet Cells) {
        if (selectedCell == null) {
            return false;
        }
        if (selectedCell.returnStone().returnPlayer() == PlayerEnum.REDQUEEN || selectedCell.returnStone().returnPlayer() == PlayerEnum.WHITEQUEEN) {
            int x = selectedCell.returnXpos();
            int y = selectedCell.returnYpos();
            PlayerEnum inversePlayer = PlayerEnum.returnInversePlayerQueen(selectedCell.returnStone().returnPlayer());
            //---- Those are the queen valid eat directions ----
            boolean result = includeValidEatCells(x, y, 1, -1, inversePlayer, Cells);
            boolean result2 = includeValidEatCells(x, y, 1, 1, inversePlayer, Cells);
            boolean result3 = includeValidEatCells(x, y, -1, 1, inversePlayer, Cells);
            boolean result4 = includeValidEatCells(x, y, -1, -1, inversePlayer, Cells);
            return result || result2 || result3 || result4;
        }
        else {
            return false;
        }
    }

    // ---- We define here the valid moves for the stones ----
    static public boolean includeValidMovementCells(Cell selectedCell, HashSet Cells) {
        // ---- If the selectedcell is null or hungrymode we can't move ----
        if (selectedCell == null || returnIfHungryMode()) {
            return false;
        }
        // ---- If the selectedcell is a queen call the includevalidmovementcellsqueen instead ----
        if ( selectedCell.returnStone().returnOnlyIfWhiteQueen() || selectedCell.returnStone().returnOnlyIfRedQueen() ){
        includeValidMovementCellsQueen(selectedCell, Cells);
            return false;
        }
        int x = selectedCell.returnXpos();
        int y = selectedCell.returnYpos();
        // ---- The yDirection will get the direction for the moves, could be -1 or 1 depends on the stone
        //      the following will be the valid moves for normal(not queen) stones ----
        int yDirection = returnVerticalDirection(selectedCell);
        boolean result = includeCellToHashSet(x + 1, y + yDirection, Cells);
        boolean result2 = includeCellToHashSet(x - 1, y + yDirection, Cells);
        return result || result2;
    }

    // ---- The same ideia of the method above, but for queen ----
    static public boolean includeValidMovementCellsQueen(Cell selectedCell, HashSet Cells) {
        if (selectedCell == null || returnIfHungryMode()) {
            return false;
        }
        int x = selectedCell.returnXpos();
        int y = selectedCell.returnYpos();
        //---- Those are the Queen valid movements ----
        boolean result =  includeCellToHashSet(x + 1, y - 1, Cells);
        boolean result2 = includeCellToHashSet(x - 1, y - 1, Cells);
        boolean result3 = includeCellToHashSet(x + 1, y + 1, Cells);
        boolean result4 = includeCellToHashSet(x - 1, y + 1, Cells);
        return result || result2 || result3 || result4;
    }

    // ---- This method will populate the movementStonesHashSet with all valid movements for the current player ----
    static private void includeValidMovementStones() {
        for (int x = 0; x < board.boardSize; x++) {
            for (int y = 0; y < board.boardSize; y++) {
                Cell cell = board.returnCellFromBoard(x, y);
                if (cell.returnOnlyIfFreeCell() || PlayerEnum.returnInversePlayer(currentPlayer) != cell.returnStone().returnPlayer() && PlayerEnum.returnInversePlayerQueen(currentPlayer) != cell.returnStone().returnPlayer()) {
                    continue;
                }
                movementStonesHashSet.add(cell);
            }
        }
    }

    // ---- The idea of this method is to count all red stones on the board to be used as statistics via TextView ----
    static public void returnRedStoneCount() {
        GameView.redStoneCount = 0;
        for (int x = 0; x < board.boardSize; x++) {
            for (int y = 0; y < board.boardSize; y++) {
                Cell cell = board.returnCellFromBoard(x, y);
                if (!cell.returnOnlyIfFreeCell() && (cell.returnOnlyIfhasPlayer(PlayerEnum.RED) || cell.returnOnlyIfhasPlayer(PlayerEnum.REDQUEEN) )) {
                    GameView.redStoneCount = GameView.redStoneCount + 1;
                }
            }
        }
    }

    // ---- The idea of this method is to count all white stones on the board to be used as statistics via TextView ----
    static public void returnWhiteStoneCount() {
        GameView.whiteStoneCount = 0;
        for (int x = 0; x < board.boardSize; x++) {
            for (int y = 0; y < board.boardSize; y++) {
                Cell cell = board.returnCellFromBoard(x, y);
                if (!cell.returnOnlyIfFreeCell() && (cell.returnOnlyIfhasPlayer(PlayerEnum.WHITE) || cell.returnOnlyIfhasPlayer(PlayerEnum.WHITEQUEEN) )) {
                    GameView.whiteStoneCount = GameView.whiteStoneCount + 1;
                }
            }
        }
    }

    static public boolean executeStoneMovement(Cell srcCell, Cell dstCell) {
        HashSet moveCells = new HashSet();
        includeValidMovementCells(srcCell, moveCells);
        if (!moveCells.contains(dstCell)) {
            return false;
        }
        // ---- When the WHITE player arrive on the row 0 after a move it becomes white queen ----
        if(currentPlayer == PlayerEnum.WHITE && dstCell.returnYpos() == 0 ) {
            dstCell.setStone(Stone.returnRedStoneQueen());
        }
        // ---- When the RED player arrive on the row boardSize-1 after a move it becomes red queen ----
        else if(currentPlayer == PlayerEnum.RED && dstCell.returnYpos() == board.boardSize -1) {
            dstCell.setStone(Stone.returnWhiteStoneQueen());
        }
        // ---- If it isn't going to become queen just define the dest stone as the source ----
        else {
            dstCell.setStone(srcCell.returnStone());
        }
        // ---- It will set the source cell as free ----
        srcCell.setFreeCell();
        // ---- It switch the players ----
        currentPlayer = PlayerEnum.returnInversePlayer(currentPlayer);
        movementStonesHashSet.clear();
        includeValidMovementStones();
        return true;
    }

    static public boolean executeStoneEating(Cell srcCell, Cell dstCell) {
        HashSet eatCells = new HashSet();
        includeValidEatCells(srcCell, eatCells);
        if (!eatCells.contains(dstCell)) {
            return false;
        }
        // ---- Eat stone in between will remove the stone which was ate ----
        eatStoneBetween(srcCell, dstCell);

        // ---- When the WHITE player arrive on the row 0 after eat a stone it becomes white queen ----
        if(currentPlayer == PlayerEnum.WHITE && dstCell.returnYpos() == 0 ) {
            dstCell.setStone(Stone.returnRedStoneQueen());
        }
        // ---- When the RED player arrive on the row boardSize-1 after eat a stone it becomes red queen ----
        else if(currentPlayer == PlayerEnum.RED && dstCell.returnYpos() == board.boardSize -1) {
            dstCell.setStone(Stone.returnWhiteStoneQueen());
        }
        // ---- If it isn't going to become queen just define the dest stone as the source ----
        else {
            dstCell.setStone(srcCell.returnStone());
        }
        // ---- It will set the source cell as free ----
        srcCell.setFreeCell();

        // ---- If there are more possible eat cells for this Stone, the player will stay playing. If not the we will switch players
        // it will be in the else rule ----
        if (includeValidEatCells(dstCell, eatCells) || includeValidEatCellsQueen(dstCell, eatCells) ) {

              // ---- We add the cell to movementStonesHashSet since this is the only valid move that
              // is available until we are out of hungryMode ----
            // ---- If we can still eat it will set hungryMode to true. ----
              if (!returnIfHungryMode()) {
                   hungryMode = true;
                }
               movementStonesHashSet.clear();
               movementStonesHashSet.add(dstCell);
               // ---- When we become queen even if there are more stones to eat it will stop the hungry mode and switch the player ----
              if ((currentPlayer == PlayerEnum.WHITE && dstCell.returnYpos() == 0 ) || (currentPlayer == PlayerEnum.RED && dstCell.returnYpos() == board.boardSize -1)){
                  hungryMode = false;
                 currentPlayer = PlayerEnum.returnInversePlayer(currentPlayer);
                 movementStonesHashSet.clear();
                 includeValidMovementStones();
              }
        }
        // ---- If we can't eat more set hungryMode to false. ----
        else {
            if (returnIfHungryMode()) {
            hungryMode = false;
            }
            // ---- It switch the players ----
            currentPlayer = PlayerEnum.returnInversePlayer(currentPlayer);
            movementStonesHashSet.clear();
            includeValidMovementStones();
        }
        return true;
    }

    static public boolean executeMovement(Cell srcCell, Cell dstCell) {
        if (!dstCell.returnOnlyIfFreeCell()) {
            return false;
        }
        // ---- If we are in hungryMode mode, we can only eat stones, movement isn't allowed ----
        if (returnIfHungryMode()) {
            return executeStoneEating(srcCell, dstCell);
        }
        // ---- It will actually execute the moving method ----
        if (executeStoneMovement(srcCell, dstCell)) {
            return true;
        }
        return false;
    }
    static public boolean executeEat(Cell srcCell, Cell dstCell) {
        // ---- If the dstCell is not empty return false, that's just a initial check ----
        if (!dstCell.returnOnlyIfFreeCell()) {
            return false;
        }
        // ---- it will actually execute the eating method ----
        if (executeStoneEating(srcCell, dstCell)) {
            return true;
        }
        else {
            return false;
        }
    }

    // ---- Eat stone in between will remove the stone which was ate
    static private Cell eatStoneBetween(Cell startCell, Cell endCell) {
        int xDirection; int yDirection;
        // ---- It will get the position of the stone between the start and end cell in the diagonal ---
        if (endCell.returnXpos() - startCell.returnXpos() > 0){
            xDirection = 1;
        }
        else {
            xDirection = -1;
        }
        if (endCell.returnYpos() - startCell.returnYpos() > 0){
            yDirection = 1;
        }
        else {
            yDirection = -1;
        }
        Cell eatCell = null;
        int x = startCell.returnXpos() + xDirection;
        int y = startCell.returnYpos() + yDirection;
        while ( x != endCell.returnXpos() ) {
            eatCell = board.returnCellFromBoard(x, y);
            if (!eatCell.returnOnlyIfFreeCell()) {
                // ---- It will define the cell which was ate as empty ----
                eatCell.setFreeCell();
            }
            x = x + xDirection;
            y = y + yDirection;
        }
        return eatCell;
    }
}