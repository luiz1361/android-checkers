package ie.martins.luiz.englishdraughts;

// ---- Enumeration class to define the players, instead of two players we're working with four players
//      however we have just two turns, one for red and another for white, the difference between the players
//      are the moves that each player can do. ----

public enum PlayerEnum {
    WHITE, RED, WHITEQUEEN, REDQUEEN;

    // ---- If the current player is WHITE it will return RED, else it will return WHITE, easy way to switch players ----
    public static PlayerEnum returnInversePlayer(PlayerEnum player) {
        if (player == WHITE) {
            return RED;
        }
        else {
            return WHITE;
        }
    }

    // ---- If it's a normal player it will return the inverse queen player, if it's a queen player it will return the inverse
    //      normal player ----
    public static PlayerEnum returnInversePlayerQueen(PlayerEnum player) {
        if (player == WHITE) {
            return REDQUEEN;
        }
        else if ( player == WHITEQUEEN) {
            return RED;
        }
        else if ( player == REDQUEEN){
            return WHITE;
        }
        else {
            return WHITEQUEEN;
        }
    }
}

